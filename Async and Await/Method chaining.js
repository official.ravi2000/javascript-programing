//Method Chaining--Asynchronous tasks to be performed one after another
new Promise(function (resolve, reject) {

    setTimeout(() => resolve(1), 1000);

}).then(function (result) {

    console.log(result);
    return new Promise(function (resolve, reject) {

        setTimeout(() => resolve(result * 2), 1000);

    })

}).then(function (result) {//handler

    console.log(result);
    return result * 2;

}).then(function (result) {

    console.log(result);
    return result * 2;

}).then(function (result) {

    console.log(result);
    return result * 2;

});
//When a handler returns a value, it becomes the result of that promise, so the next .then is called with it.


// const promise = new Promise(function (resolve, reject) {

//     setTimeout(() => resolve(1), 1000);

// });

// promise.then(function (result) {
//     console.log(result);
//     return result * 2;
// });

// promise.then(function (result) {
//     console.log(result);
// });
