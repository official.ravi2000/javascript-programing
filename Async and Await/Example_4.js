// function getData() {
//     let handlePromise = new Promise((resolve,reject)=>{
//         setTimeout(() => {
//             console.log("Set TimeOut");
//             resolve("All done")
//         }, 2000);
//     })
//     handlePromise.then((result)=>{
//         console.warn(result)
//     })
// }
// getData(); 

async function getData() {
    let handlePromise = new Promise((resolve,reject)=>{
        setTimeout(() => {
            console.log("Set TimeOut");
            resolve("All done")
        }, 2000);
    })
    let result =await handlePromise;
    console.log(result);
}
getData(); 