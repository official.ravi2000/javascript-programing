// async function myFunction() {
//     console.log("inside calling myFunction ");
//     return "Ravindra Baranjalekar";
// }

// console.log("Before calling myFunction()");
// let variable= myFunction();
// console.log("after calling myFunction()");
// console.log(variable);
// console.log("Last Line of this Js file");





async function myFunction() {
    console.log("inside calling myFunction ");

    const result = await fetch('https://api.github.com/users');
    console.log('before result');
    const users = await result.json();
    console.log('user resolved');
    return users;

}

console.log("Before calling myFunction()");
let variable = myFunction();
console.log("after calling myFunction()"); 
console.log(variable);
variable.then(data => console.log(data));
console.log("Last Line of this Js file");