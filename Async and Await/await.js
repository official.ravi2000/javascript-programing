//await 



async function asyncFuction() {
    let promise = new Promise((resolve, reject) => {
        setTimeout(() => resolve("Promise Resolved", 3000));
    });

    let wait = await promise;
    console.log(wait);
}

console.log(asyncFuction());

//  function asyncFuction2() {
//     let promise = new Promise((resolve,reject)=>{
//         setTimeout(()=>reject("resolved",1000));
//     })

//     let wait = await promise;
//     console.log(wait);
// }

// asyncFuction2();


