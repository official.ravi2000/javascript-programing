
// let {sayGM, sayGN,sayGoodafternon} = await import('./say.js');
let date = new Date();
let hours = date.getHours();

if(hours<12){
    let {sayGM} = await import('./GreetMessage.js');
    sayGM();
}else if(hours>12 && hours < 18){
    let {sayGoodafternon} = await import('./GreetMessage.js');
    sayGoodafternon();
}else{
    let {sayGoodEvening} = await import('./GreetMessage.js');
    sayGoodEvening();
}

