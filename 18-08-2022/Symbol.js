//Symbol
//A “symbol” represents a unique identifier.


const sym2 = Symbol('foo');
const sym3 = Symbol('foo');

console.log(sym2 == sym3);

console.log(sym3);

console.log(sym3.description);