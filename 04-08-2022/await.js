//await 



async function asyncFuction() {
    let promise = new Promise((resolve,reject)=>{
        setTimeout(()=>resolve("resolved",1000));
    })

    let wait = await promise;
    console.log(wait);
}

asyncFuction();


//  function asyncFuction2() {
//     let promise = new Promise((resolve,reject)=>{
//         setTimeout(()=>reject("resolved",1000));
//     })

//     let wait = await promise;
//     console.log(wait);
// }

// asyncFuction2();


