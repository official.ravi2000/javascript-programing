function normalFunc() {
  console.log('from mormal function ')
  return "Hello Riya";
}


function* generatorFunction() {

  yield normalFunc();
  console.log('I will be printed after the pause');
  yield '...!!';
}
const generatorObject = generatorFunction();

console.log(generatorObject.next().value);
console.log("How Are you ?");
console.log(generatorObject.next().value);
console.log(generatorObject.next());