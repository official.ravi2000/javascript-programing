// const iterableObj = {
//     [Symbol.iterator]() {
//         let step = 0;
//         return {
//             next() {
//                 step++;
//                 if (step === 1) {
//                     return { value: 'Hello', done: false };
//                 } else if (step === 2) {
//                     return { value: 'Users', done: false };
//                 } else if (step === 3) {
//                     return { value: '....!', done: false };
//                 }
//                 return {
//                     value: undefined,
//                      done: true
//                 };
//             }
//         }
//     },
// }
// for (const val of iterableObj) {
//     console.log(val);
// }




function * iterableObj() {
    yield 'This';
    yield 'is';
    yield 'iterable.'
  }
  for (const val of iterableObj()) {
    console.log(val);
  }