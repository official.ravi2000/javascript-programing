var fruits = ["Apples", "Oranges", "Grapes"];

// fruits.forEach(element => {
//     console.log(element);
// });

// for (const iterator of fruits) {
//     console.log(iterator);
// }

let myFruits = fruits[Symbol.iterator]();

console.log(typeof fruits[Symbol.iterator]);

console.log(myFruits.next());
console.log(".................");
myFruits.next();
