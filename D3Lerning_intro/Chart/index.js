//import { select,csv } from 'd3';

 //const svg = d3.select('svg');

// const width = +svg.attr('width');
// const height = +svg.attr('height');

// const render = data=>{
//     svg.selectAll('rect').data(data)
//     .enter().append('rect')
//     .attr('width',300)
//     .attr('height',300);
// }



// d3.csv('data.csv').then((data) => {
//     data.forEach(element => {
//         element.population =+element.population;
//     });
//     console.log(data);
//     render(data);
// });

//overlaping

//===========================================================



// const svg = d3.select('svg');

// const width = +svg.attr('width');
// const height = +svg.attr('height');

// const render = data => {
//     const xscale = d3.scaleLinear()
//         .domain([0, d3.max(data,d=>d.population)])
//         .range([0,width]);

//         //separate the bar to represent there height
//         const yScale= d3.scaleBand()
//         .domain(data.map(d=>d.country))
//         .range([0,height]);//range from top to bottom


//         console.log(yScale.domain());


//         console.log(xscale.domain() );// select highest population country
//         //console.log(xscale.range() );
//     svg.selectAll('rect').data(data)
//         .enter().append('rect')

//         .attr("fill", "orange")
//         .attr('y',d=> yScale(d.country))
//         .attr('width', d=>xscale(d.population))
//        // .attr('height', yScale.bandwidth());//computed with of single bar
//         .attr('height',yScale.bandwidth());//computed with of single bar
// }//bandwidth () function in d3.js is used to find the width of each band



// d3.csv('data.csv').then((data) => {
//     data.forEach(element => {
//         element.population = +element.population*1000;
//     });
//     //console.log(data);
//     render(data);
// });

//==================================================================================================================

//The Marging Convention

//pixel





// const svg = d3.select('svg');

// const width = +svg.attr('width');
// const height = +svg.attr('height');

// const render = data => {

//     xValue = d => d.population;
//     yValue = d => d.country;
//     const margin = { top: 20, right: 20, bottom: 20, left: 20 };

//     const innerWidth = width - margin.left - margin.right;
//     const innerHeight = height - margin.top - margin.bottom;

//     console.log(innerHeight);
//     console.log(innerWidth);
//     const xscale = d3.scaleLinear()
//         .domain([0, d3.max(data, xValue)])
//         .range([0, innerWidth]);

//     //separate the bar to represent there height
//     const yScale = d3.scaleBand()
//         .domain(data.map(yValue))
//         .range([0, innerHeight]);//range from top to bottom


//     const g = svg.append('g')
//         .attr('transform', `translate(${margin.left},${margin.top})`);
//     //translate(x,y)




//     g.selectAll('rect').data(data)
//         .enter().append('rect')

//         .attr("fill", "orange")
//         .attr('y', d => yScale(yValue(d)))
//         .attr('width', d => xscale(xValue(d)))
//         // .attr('height', yScale.bandwidth());//computed with of single bar
//         .attr('height', yScale.bandwidth());//computed with of single bar
// }//bandwidth () function in d3.js is used to find the width of each band



// d3.csv('data.csv').then((data) => {
//     data.forEach(element => {
//         element.population = +element.population * 1000;
//     });

//     render(data);
// });


//=========================================================


//adding the axis





const svg = d3.select('svg');

const width = +svg.attr('width');
const height = +svg.attr('height');

const render = data => {

    xValue = d => d.population;
    yValue = d => d.country;
    const margin = { top: 20, right: 20, bottom: 20, left: 200 };

    const innerWidth = width - margin.left - margin.right;
    const innerHeight = height - margin.top - margin.bottom;

    console.log(innerHeight);
    console.log(innerWidth);
    const xscale = d3.scaleLinear()
        .domain([0, d3.max(data, xValue)])
        .range([0, innerWidth]);

        
        
        //separate the bar to represent there height
        const yScale = d3.scaleBand()
        .domain(data.map(yValue))
        .range([0, innerHeight])//range from top to bottom
        .padding(0.3);//for separating bar
       
        

    const g = svg.append('g')
        .attr('transform', `translate(${margin.left},${margin.top})`);
    //translate(x,y)


    //AXIS
    
    const yAxis = d3.axisLeft(yScale);
    yAxis(g.append('g'));
    g.append('g').call(d3.axisBottom(xscale))
    .attr('transform', `translate(0,${innerHeight})`)



    g.selectAll('rect').data(data)
        .enter().append('rect')

        .attr("fill", "orange")
        .attr('y', d => yScale(yValue(d)))
        .attr('width', d => xscale(xValue(d)))
        // .attr('height', yScale.bandwidth());//computed with of single bar
        .attr('height', yScale.bandwidth());//computed with of single bar
}//bandwidth () function in d3.js is used to find the width of each band



d3.csv('data.csv').then((data) => {
    data.forEach(element => {
        element.population = +element.population * 1000;
    });

    render(data);
});