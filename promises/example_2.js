const one = () => {
    return "I am One";
}
const two = () => {
    return new Promise((resolve, reject) => {
        resolve("I am Two");
    })
}
// const two = () => {
//     setTimeout(() => {
//         return "I am Two";
//     }, 3000);
// }
const three = () => {
    return "I am Three";
}

const callMe = () => {
    let valOne = one();
    console.log(valOne);

    let valTwo = two();
    console.log(valTwo);

    let valThree = three();
    console.log(valThree);
}
callMe();