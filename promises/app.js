//A promises is an object that returns a value which you hope to receive in the future, but not now

//fulfilled
// let data = new Promise((resolve,reject)=>{
//     setTimeout(() => {
//         resolve({name:`ravi`});
//     });
// });

// console.log(data);

//pending
// let data = new Promise((resolve,reject)=>{
//     setTimeout(() => {
//         resolve({name:`ravi`});
//     },2000);
// });

// data.then((item)=>{
//     console.log(item);
// });
// console.log(data);
// console.log("Welcome");


//rejected
// let data = new Promise((resolve,reject)=>{
//     setTimeout(() => {
//         reject("some issues");
//     },2000);
// });

// data.then((item)=>{
//     console.warn(item)
// });

// data.then((item)=>{
//     console.warn(item)
// }).catch((err)=>{
//     console.log("catch Block",err);
// }).finally(()=>{
//     console.log("finally block executed");
// });

let data = fetch("https://dummy.restapiexample.com/api/v1/employees");

// data.then((item)=>{//promise for resolving API
//     console.warn(item)
// })

//promise chaning 
data.then((item)=>{
    return item.json();
}).then((result)=>{
    console.log(result);
})

