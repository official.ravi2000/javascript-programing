// There are two types of call back function

/*
Synchronous  - It waits for each operation to complete, and after that next operation will executes.

Asynchonous - It will never wait for each operation to complete.



*/

function randomFunction() {
    setTimeout(() => {
        console.log("ramdom Function");
    }, 5000);
}

randomFunction();

console.log('End');
