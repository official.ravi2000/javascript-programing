//callback passed into another funtion as an argument which is invoke inside the outer function
// to complete some king of routine or action

function show(){// 
    console.log("I am in show Function");
}
function myFunction(callback) {
    console.log(`I am in myFunction`);
    callback();
}
myFunction(show);// show function will calback function